package main

import (
	"fmt"
	"os"

	"gitlab.com/SuperMatt/DNSoverHTTPS/server"
)

func main() {
	if len(os.Args) == 1 {
		printHelp()
	}

	switch os.Args[1] {
	case "server":
		server.Start(os.Args[2:])
	default:
		printHelp()
	}
}

func printHelp() {
	fmt.Printf("usage: %s <command> [args]\n", os.Args[0])
	fmt.Println("The most common commands are:")
	fmt.Println("  server  Start a DNS over HTTPS server")
	fmt.Println("  client  Start a DNS over HTTPS client")
	os.Exit(2)
}