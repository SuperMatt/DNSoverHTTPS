package server

import (
	"encoding/base64"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/miekg/dns"
)

var (
	unsupportedMethod = "unsupported method"
)

func handleRequest(r *http.Request) (m *dns.Msg, err error) {
	if r.Method == "GET" {
		return handleMethodGET(r)
	} else if r.Method == "POST" {
		return handleMethodPOST(r)
	}

	return m, errors.New(unsupportedMethod)
}

func handleMethodGET(r *http.Request) (m *dns.Msg, err error) {
	if r.FormValue("name") != "" && r.FormValue("type") != "" {
		return handleFormatJSON(r), nil
	} else if r.FormValue("dns") != "" {
		return handleFormatWire(r)
	}

	return m, errors.New("incorrect GET form. either use DNS or name and type")
}

func handleMethodPOST(r *http.Request) (m *dns.Msg, err error) {
	if r.FormValue("name") != "" && r.FormValue("type") != "" {
		return handleFormatJSON(r), nil
	}
	return handleFormatWire(r)
}

func handleFormatJSON(r *http.Request) (m *dns.Msg) {
	m = new(dns.Msg)

	name := r.FormValue("name")
	t := dns.StringToType[r.FormValue("type")]
	m.SetQuestion(dns.Fqdn(name), t)
	return m
}

func handleFormatWire(r *http.Request) (m *dns.Msg, err error) {
	var b []byte

	m = new(dns.Msg)

	if r.Method == "GET" {
		b, err = base64.StdEncoding.DecodeString(r.FormValue("dns"))
		if err != nil {
			return m, err
		}
	} else if r.Method == "POST" {
		defer r.Body.Close()
		b, err = ioutil.ReadAll(r.Body)
		if err != nil {
			return m, err
		}
	} else {
		return m, errors.New(unsupportedMethod)
	}

	err = m.Unpack(b)
	if err != nil {
		return m, err
	}

	return m, nil
}
