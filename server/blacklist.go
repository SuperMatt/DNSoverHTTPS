package server

import (
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/miekg/dns"

	log "github.com/sirupsen/logrus"
)

var (
	blacklistMap sync.Map
)

func startBlacklistSync(b *string, t *string) {
	td, err := time.ParseDuration(*t)
	if err != nil {
		log.Fatal(err)
	}
	if len(*b) == 0 {
		log.Info("No blacklist provided")
	}

	go blacklistSync(*b, td)
}

func blacklistSync(b string, t time.Duration) {
	for {
		r, err := http.Get(b)
		if err != nil {
			log.WithField("Err", err).Warn("Unable to pull blacklist")
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.WithField("Err", err).Warn("Unable to read blacklist body")
		}

		blacklistLines := strings.Split(string(body), "\n")

		for _, line := range blacklistLines {

			if line == "" {
				continue
			}

			_, ok := blacklistMap.LoadOrStore(dns.Fqdn(line), true)
			if !ok {
				log.WithField("domain", line).Debug("Loaded into blacklist")
			}
		}

		time.Sleep(t)
	}

}

func isInBlacklist(d string) bool {

	_, ok := blacklistMap.Load(dns.Fqdn(d))
	if ok {
		log.WithField("domain", d).Debug("Found domain in blacklist")
	} else {
		log.WithField("domain", d).Debug("Could not find domain in blacklist")
	}
	return ok
}
