package server

import (
	"flag"
	"fmt"
	"net"
	"net/http"

	"github.com/miekg/dns"
	log "github.com/sirupsen/logrus"
)

const (
	defaultSubdir string = "/dns-query"
)

var (
	dnserver      string
	c             *dns.Client
	cacheDisabled bool
)

//Start the server with an array of strings as flags
func Start(args []string) {

	serverFlags := flag.NewFlagSet("server", flag.ExitOnError)
	serverDNSEndpoint := serverFlags.String("dnserver", "1.1.1.1:53", "Domain name server to use (must be ip address)")
	listenAddress := serverFlags.String("listen", ":8080", "Listen address")
	subDirectory := serverFlags.String("subdir", defaultSubdir, "Subdirectory on which to listen (cannot be /)")
	nocache := serverFlags.Bool("nocache", false, "Disables caching")
	tlsKey := serverFlags.String("key", "", "TLS Key location")
	tlsCert := serverFlags.String("cert", "", "TLS Cert location")
	debug := serverFlags.Bool("debug", false, "Debug mode")
	blacklistURL := serverFlags.String("blacklist-url", "https://pgl.yoyo.org/adservers/serverlist.php?showintro=0;hostformat=plain;mimetype=plaintext", "URL of blacklisted domains, similar to https://pgl.yoyo.org/adservers/serverlist.php?showintro=0;hostformat=plain;mimetype=plaintext")
	blacklistDuration := serverFlags.String("backlist-duration", "1h", "Time between blacklist reloads")

	serverFlags.Parse(args)

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	if *subDirectory == "/" {
		log.Warn(fmt.Sprintf("Subdir cannot be /, defaulting to %s", defaultSubdir))
		ds := defaultSubdir
		subDirectory = &ds

	} else if *subDirectory == "/stats" {
		log.Warn(fmt.Sprintf("Subdir cannot be /stats, defaulting to %s", defaultSubdir))
		ds := defaultSubdir
		subDirectory = &ds
	}

	if _, _, err := net.SplitHostPort(*listenAddress); err != nil {
		log.Fatal(fmt.Sprintf("%s is not a valid host and port", *listenAddress))
	}

	// TODO add some logic to make sure there is a host and port for dns server

	dnserver = *serverDNSEndpoint
	cacheDisabled = *nocache

	c = new(dns.Client)

	startBlacklistSync(blacklistURL, blacklistDuration)
	startStats()
	startServer(serverDNSEndpoint, listenAddress, subDirectory, tlsKey, tlsCert)
}

func startServer(dnsEndpoint, listenAddress, subDirectory, tlsKey, tlsCert *string) {

	http.HandleFunc(*subDirectory, dnsQueryHandler)
	http.HandleFunc("/.well-known/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		send(w, "")
	})

	http.HandleFunc("/stats/", statsHandler)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		send(w, "")
	})

	log.Info("Starting server")
	if *tlsKey != "" && *tlsCert != "" {
		log.Fatal(http.ListenAndServeTLS(*listenAddress, *tlsCert, *tlsKey, nil))
	} else {
		log.Fatal(http.ListenAndServe(*listenAddress, nil))
	}
}
