package server

import (
	"encoding/json"

	"github.com/miekg/dns"
)

type queryJSON struct {
	Status   int
	TC       bool
	RD       bool
	RA       bool
	AD       bool
	CD       bool
	Question []questionJSON
	Answer   []answerJSON
}

func (q *queryJSON) String() string {
	s, err := json.Marshal(*q)
	if err != nil {
		return err.Error()
	}

	return string(s)
}

type questionJSON struct {
	Name string
	Type uint16
}

type answerJSON struct {
	Name string
	Type uint16
	TTL  uint32
	Data string
}

func msgToJSON(m *dns.Msg) string {
	var j queryJSON

	j.AD = m.AuthenticatedData
	j.CD = m.CheckingDisabled
	j.RA = m.RecursionAvailable
	j.RD = m.RecursionDesired
	j.TC = m.Truncated
	for _, question := range m.Question {
		var qJ questionJSON
		qJ.Name = question.Name
		qJ.Type = question.Qtype
		j.Question = append(j.Question, qJ)
	}

	for _, answer := range m.Answer {
		var aJ answerJSON
		aJ.Name = answer.Header().Name
		aJ.TTL = answer.Header().Ttl
		aJ.Type = answer.Header().Rrtype

		for i := 1; i <= dns.NumField(answer); i++ {
			spacer := " "
			if i == 1 {
				spacer = ""
			}

			aJ.Data += spacer + dns.Field(answer, i)
		}

		j.Answer = append(j.Answer, aJ)
	}

	return j.String()
}
