package server

import (
	"fmt"
	"net/http"
)

func send(w http.ResponseWriter, s string) {
	w.Header().Set("X-Clacks-Overhead", "GNU Terry Pratchett")
	fmt.Fprintf(w, s)

}
