package server

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/miekg/dns"
)

var (
	globalCache sync.Map
)

type cache struct {
	Data    *dns.Msg
	Invalid time.Time
}

func makeMapAddress(recordName string, t uint16) string {
	s := fmt.Sprintf("%s_%v", recordName, t)
	return s
}

func getFromCache(recordName string, t uint16) (m *dns.Msg, err error) {
	mapAddress := makeMapAddress(recordName, t)

	result, ok := globalCache.Load(mapAddress)
	if ok == false {
		return m, errors.New("Entry not found in cache")
	}

	r := result.(cache)

	if r.Invalid.Before(time.Now()) {
		return m, errors.New("Cache invalid")
	}

	newTTL := time.Until(r.Invalid)
	newTTLSeconds := uint32(newTTL.Seconds())
	for k := range r.Data.Answer {
		r.Data.Answer[k].Header().Ttl = newTTLSeconds
	}

	return r.Data, nil
}

func saveToCache(recordName string, t uint16, m *dns.Msg) error {
	mapAddress := makeMapAddress(recordName, t)
	var c cache
	c.Data = m
	d, err := time.ParseDuration(fmt.Sprintf("%vs", m.Answer[0].Header().Ttl))
	if err != nil {
		return err
	}
	c.Invalid = time.Now().Add(d)
	globalCache.Store(mapAddress, c)
	return nil
}
