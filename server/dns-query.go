package server

import (
	"net/http"

	"github.com/miekg/dns"
	log "github.com/sirupsen/logrus"
)

func dnsQueryHandler(w http.ResponseWriter, r *http.Request) {
	log.WithField("method", r.Method).Debug("Received Request")
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		send(w, err.Error())
		return
	}

	ct := r.Header.Get("content-type")
	at := r.Header.Get("accept")

	jf := "application/dns-json"

	jsonResponse := false

	if ct == jf || at == jf {
		jsonResponse = true
	}

	out, err := handleRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Debug(err.Error())
		send(w, err.Error())
		return
	}

	n := out.Question[0].Name
	ti := out.Question[0].Qtype
	ts := dns.TypeToString[ti]

	if isInBlacklist(n) {
		var o string
		if jsonResponse {
			w.Header().Set("content-type", "application/dns-json")
			o = msgToJSON(out)
		} else {
			w.Header().Set("content-type", "application/dns-message")
			b, err := out.Pack()
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				send(w, err.Error())
				return
			}

			o = string(b)
		}
		send(w, o)
		return

	}

	addStatCounter(n, ts)
	logRequestDetails(n, r.UserAgent())

	if !cacheDisabled {
		logNameTypeDebug(n, ts, "Checking for cache")

		cachedData, err := getFromCache(n, ti)
		if err != nil {
			log.Debug(err)
		} else {
			logNameTypeDebug(n, ts, "Found cached record, sending...")
			if jsonResponse {
				send(w, msgToJSON(cachedData))
				return
			}
			b, err := cachedData.Pack()
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				send(w, err.Error())
				return
			}
			send(w, string(b))
			return
		}
	}

	in, _, err := c.Exchange(out, dnserver)
	if err != nil {
		logNameTypeDebug(n, ts, "Unable to lookup record")
		w.WriteHeader(http.StatusInternalServerError)
		send(w, err.Error())
		return
	}

	var o string

	if jsonResponse {
		w.Header().Set("content-type", "application/dns-json")
		o = msgToJSON(in)
	} else {
		w.Header().Set("content-type", "application/dns-message")
		b, err := in.Pack()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			send(w, err.Error())
			return
		}

		o = string(b)
	}

	if !cacheDisabled {
		if len(in.Answer) > 0 {
			logNameTypeTTLDebug(n, ts, in.Answer[0].Header().Ttl, "Saving cache")
			saveToCache(n, ti, in)
		}
	}

	send(w, o)
}
