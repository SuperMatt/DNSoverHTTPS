package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
)

type statChannelLog struct {
	Name string
	Type string
}

type statObject struct {
	Count uint64
	Name  string
	Type  string
}

var (
	ch    chan statChannelLog
	stats sync.Map
)

func startStats() {
	ch = make(chan statChannelLog)
	go statScraper()
}

func statScraper() {
	for {
		s := <-ch
		mapName := fmt.Sprintf("%s_%s", s.Name, s.Type)
		i, ok := stats.Load(mapName)
		var obj statObject
		if ok {
			obj = i.(statObject)
			obj.Count = obj.Count + 1

		} else {
			obj = statObject{Count: 1, Name: s.Name, Type: s.Type}

		}

		stats.Store(mapName, obj)

	}
}

func addStatCounter(m, t string) {
	l := statChannelLog{Name: m, Type: t}

	ch <- l
}

func statsHandler(w http.ResponseWriter, r *http.Request) {
	var s []statObject
	stats.Range(func(key, value interface{}) bool {
		s = append(s, value.(statObject))
		return true
	})

	b, _ := json.Marshal(s)
	w.Header().Set("content-type", "application/json")
	send(w, string(b))
}
