package server

import log "github.com/sirupsen/logrus"

func logRequestDetails(fqdn, ua string) {
	log.WithFields(log.Fields{"FQDN": fqdn, "UA": ua}).Debug("Request Details")
}

func logNameTypeDebug(name, t, message string) {
	log.WithFields(log.Fields{
		"name": name,
		"type": t,
	}).Debug(message)
}

func logNameTypeTTLDebug(name string, t string, ttl uint32, message string) {
	log.WithFields(log.Fields{
		"name": name,
		"type": t,
		"ttl":  ttl,
	}).Debug(message)
}
